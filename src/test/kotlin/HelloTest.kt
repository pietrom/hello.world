package com.darwinsw.hello

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test

class HelloTest {
    @Test
    fun sayHiToWorld() {
        assertThat(Hello("Hi").sayHello("World"), `is`(equalTo("Hi, World!")))
    }
}