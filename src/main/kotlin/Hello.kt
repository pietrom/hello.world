package com.darwinsw.hello

class Hello(private val greetings: String) {
    fun sayHello(to: String) = "${greetings}, ${to}!"
}